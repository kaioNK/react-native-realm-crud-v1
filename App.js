import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {createStackNavigator, createAppContainer} from 'react-navigation'

import InitialPage from './src/Components/InitialPage'
import InsertUsers from './src/Components/InsertUsers'


const Realm = require('realm')

const AppNavigator = createStackNavigator(
  {
    TelaInicial: {screen: InitialPage},
    TelaInserirUsuarios: {screen: InsertUsers}
  },
  {
    initialRouteName: 'TelaInicial'
  }
)

const AppContainer = createAppContainer(AppNavigator)

export default class App extends Component{
  componentDidMount(){
    console.log('REALM PATH: ', Realm.defaultPath)
  }
  
  render(){
    return(
      <AppContainer />
    )
  }
}

