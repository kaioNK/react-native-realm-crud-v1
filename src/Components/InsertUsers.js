import React, {Component} from 'react'
import {View, Alert, StyleSheet, TextInput, Text, TouchableOpacity} from 'react-native'
import {Header} from 'react-native-elements'

import {UserSchema} from '../Database/realm'

//import  {realmInstance as realm} from '../Database/realm.js'

const Realm = require('realm')
let realm

class InsertUsers extends Component{
  static navigationOptions = {header: null}

  constructor(props){
    super(props)

    this.state = {
      user_firstname: '',
      user_lastname: '',
      user_email: ''
    }

    realm = new Realm({schema: [UserSchema]})
  }

  insertUser = () => {
    realm.write(() => {
      let ID = realm.objects('User').length + 1
      realm.create('User', {
        id: ID,
        firstname: this.state.user_firstname,
        lastname: this.state.user_lastname,
        email: this.state.user_email
      })
    })
    Alert.alert('Usuário inserido com sucesso!')
  }

  render(){
    let getInfo = realm.objects('User')
    let myJSON = JSON.stringify(getInfo)
    console.log(myJSON)
    return(
      <View>
        <Header 
          centerComponent={{text: 'ADICIONAR USUÁRIO', style: {color: '#fff', fontSize: 24}}}
          backgroundColor='green'
        />
          <View style={styles.container}>
          <TextInput
            placeholder="Digite o nome..."  
            style={styles.textInputStyle}
            underlineColorAndroid='transparent'
            onChangeText={(text) => {this.setState({user_firstname: text})}}
          />

          <TextInput 
            placeholder='Digite o sobrenome...'
            style={styles.textInputStyle}
            underlineColorAndroid='transparent'
            onChangeText={(text) => {this.setState({user_lastname: text})}}
          />

          <TextInput
            placeholder='Digite o email...'
            keyboardType='email-address'
            style={styles.textInputStyle}
            underlineColorAndroid='transparent'
            onChangeText={(text) => {this.setState({user_email: text})}}
          />

          <TouchableOpacity
            onPress={this.insertUser}
            style={styles.button}
          >
            <Text style={styles.textStyle}>ADICIONAR</Text>
          </TouchableOpacity>

        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 120,
    margin: 30
  },
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#009688',
    width: '100%',
    height: 50,
    borderRadius: 10,
    marginBottom: 20,
    textAlign: 'center'
  },
  button: {
    width: '80%',
    height: 50,
    padding: 10,
    backgroundColor: '#4caf50',
    borderRadius: 7,
    paddingTop: 10
    //marginTop: 10
  },
  textStyle: {
    fontSize: 20,
    textAlign: 'center',
    //justifyContent: 'center',
    color: 'white'
  }
  
})

export default InsertUsers 