import React, {Component} from 'react'
import {View, Text, TouchableOpacity, StyleSheet, Platform, ImageBackground} from 'react-native'
import {Avatar} from 'react-native-elements'

import bgImage from '../Images/sky.jpeg'

export default class InitialPage extends Component{

  static navigationOptions = {
    //title: 'Tela Inicial',
    header: null
  }

  render(){
    const {navigate} = this.props.navigation
    return(
      <ImageBackground source={bgImage} style={{width: '100%', height: '100%'}}>
        <View style={styles.container}>
          <Avatar 
            size='large'
            rounded
            icon={{name: 'account-plus', type: 'material-community', color:'green', size: 48}}
            onPress={() => navigate('TelaInserirUsuarios')}
          />
          <Text style={styles.textStyle}>
           Adicionar Usuário
          </Text>
        </View>

        <View style={styles.container}>
          <Avatar
            size='large'
            rounded
            icon={{name: 'account-multiple', type: 'material-community', color:'orange', size: 48}}
            onPress={() => navigate('TelaListarUsuarios')}
          />
          <Text style={styles.textStyle}>
            Listar Usuários
          </Text>
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    margin: 10
  },
  textStyle: {
    color: '#fff',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 24,
    marginTop: 10
  },
  button: {
    width: '50%',
    height: 50,
    padding: 10,
    backgroundColor: '#4caf50',
    borderRadius: 7,
    marginTop: 12
  }
})
