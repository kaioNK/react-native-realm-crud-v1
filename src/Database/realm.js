
const UserSchema = {
  name: 'User',
  primaryKey: 'id',
  properties: {
    id: {type: 'int', default: 0},
    firstname: 'string',
    lastname: 'string',
    email: 'string'
  }
}

export {UserSchema}